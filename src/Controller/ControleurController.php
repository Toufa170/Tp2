<?php

namespace App\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\Routing\Annotation\Route;

class ControleurController extends AbstractController
{
    /**
     * @Route("/controleur", name="controleur")
     */
    public function index() // tableau dans tableau
    {
        return $this->render('controleur/index.html.twig', [
            'controller_name' => 'ControleurController',
            'infothese' =>  [
                ['titre'=>'COMPACTION DE SÉDIMENTS PAR FLUIDISATION ','contact'=>'fatim07100gmail.com','Description' =>'Comment empiler des sphères de manière la plus compacte est une question qui a engendré un grand nombre d’études fondamentales depuis la conjoncture de Kepler jusqu’a la théorie d’Edward [1] dans laquelle le rôle joué par l’énergie dans les systèmes statistiques est remplacé par le volume occupé pour des grains forcément athermique.
                    Depuis les travaux d’Edward de nombreuses expériences ont été conduites pour étudier la compactivité d’un empilement de sphères. Bien que peu d’études se soient intéressées à des matériaux granulaires immergés, ces derniers présentent un comportement différents de celui des matériaux secs.'], 
            ['titre'=>'Dynamique des nappes minces visqueuses: Theorie, modelisation et experiences en laboratoire','contact'=>'fatoumata-f-traore@etu.umontpellier.fr', 'Description' => 'Des minces nappes de fluide visqueux se trouvent dans bon nombre de situations naturelles, industrielles, et de la vie courante. Le but du sujet propose est de developper une nouvelle methode de simulation numerique de la dynamique de ces objets.'],
            [ 'titre'=> 'Guides à cristaux photoniques pour linteraction forte atome-photon','contact'=> 'fatim0710yahoo.fr','Description' => 'Designer, fabriquer et caractériser des guides à modes lents avec une architecture compatible avec nuage datome froids']


            ]
        ]);
    }
}
